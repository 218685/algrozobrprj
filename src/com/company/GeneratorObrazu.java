package com.company;

import java.util.Random;

/**
 * Created by agasinsk on 15.03.17.
 */
public class GeneratorObrazu {

    private Random gen;

    //prawdop. losowego wyboru klas
    private float p1, p2;
    //parametry dla rozkladow
    private float a1, b1, a2, b2;

    private boolean rozkladGaussa;

    GeneratorObrazu() {

        gen = new Random();
        p1 = 0.5f;
        p2 = 1 - p1;
    }

    GeneratorObrazu(float p1) {

        gen = new Random();
        this.p1 = p1;
        this.p2 = 1 - p1;
    }

    GeneratorObrazu(float p1, boolean rozkladGaussa) {

        gen = new Random();
        this.p1 = p1;
        this.p2 = 1 - p1;
        this.rozkladGaussa = rozkladGaussa;
    }

    public float getP1() {
        return p1;
    }

    public float getP2() {
        return p2;
    }

    public float getA1() {
        return a1;
    }

    public float getB1() {
        return b1;
    }

    public float getA2() {
        return a2;
    }

    public float getB2() {
        return b2;
    }

    public boolean isRozkladGaussa() {
        return rozkladGaussa;
    }

    public void setRozkladGaussa(boolean rozkladGaussa) {
        this.rozkladGaussa = rozkladGaussa;
    }

    public void setParametryRozkladu(float a1, float b1, float a2, float b2) {
        this.a1 = a1;
        this.b1 = b1;
        this.a2 = a2;
        this.b2 = b2;
    }

    public Obraz generujObraz() {

        int klasaObrazu;
        float wartoscObrazu = 0;
        float generowanaKlasa = gen.nextFloat();
        float p1 = this.getP1();
        if (generowanaKlasa <= p1)
            klasaObrazu = 1;
        else
            klasaObrazu = 2;

        float wynikLosowania;

        if (rozkladGaussa) {
            wynikLosowania = (float) gen.nextGaussian();
            if (klasaObrazu == 1)
                wartoscObrazu = b1 * wynikLosowania + a1;
            if (klasaObrazu == 2)
                wartoscObrazu = b2 * wynikLosowania + a2;
        } else {
            wynikLosowania = gen.nextFloat();
            if (klasaObrazu == 1)
                wartoscObrazu = (b1 - a1) * wynikLosowania + a1;
            if (klasaObrazu == 2)
                wartoscObrazu = (b2 - a2) * wynikLosowania + a2;
        }

        return new Obraz(klasaObrazu, wartoscObrazu);
    }

}
