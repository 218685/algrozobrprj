package com.company;

public interface ExtComparable<T> extends Comparable<T> {
    public double distanceTo(T other);
}
