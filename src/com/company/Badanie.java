package com.company;

public class Badanie {

    private AlgorytmTester tester = new AlgorytmTester();

    public static void main(String[] args) {

        Badanie badanie = new Badanie();
        //badanie.testujAlgorytmy();

        badanie.testujAlgorytmyTylkoCiagUczacy();

        badanie.testujAlgorytmyTylkoCiagTestowy();
    }

    private void testujAlgorytmy() {
        //kombinacje parametrow dla rozkladu jednostajnego - nienachodzace na siebie
        tester.testujAlgorytmy(0.1f, false, 3, 6, 7, 9);
        tester.testujAlgorytmy(0.5f, false, 3, 6, 7, 9);
        tester.testujAlgorytmy(0.8f, false, 3, 6, 7, 9);

        //lekko nachodzace na siebie
        tester.testujAlgorytmy(0.1f, false, 3, 6, 5.5f, 9);
        tester.testujAlgorytmy(0.5f, false, 3, 6, 5.5f, 9);
        tester.testujAlgorytmy(0.8f, false, 3, 6, 5.5f, 9);

        //mocno nachodzace na siebie
        tester.testujAlgorytmy(0.1f, false, 3, 6, 4, 9);
        tester.testujAlgorytmy(0.5f, false, 3, 6, 4, 9);
        tester.testujAlgorytmy(0.8f, false, 3, 6, 4, 9);

        //kombinacje parametrow dla rozkladu Gaussa - nienachodzace na siebie
        tester.testujAlgorytmy(0.1f, true, -2, 0.5f, 7, 0.5f);
        tester.testujAlgorytmy(0.5f, true, -2, 0.5f, 7, 0.5f);
        tester.testujAlgorytmy(0.8f, true, -2, 0.5f, 7, 0.5f);

        //lekko nachodzace na siebie
        tester.testujAlgorytmy(0.1f, true, -2, 1, 1, 1);
        tester.testujAlgorytmy(0.5f, true, -2, 1, 1, 1);
        tester.testujAlgorytmy(0.8f, true, -2, 1, 1, 1);

        //mocno nachodzace na siebie
        tester.testujAlgorytmy(0.1f, true, -4, 7, 1, 2.25f);
        tester.testujAlgorytmy(0.5f, true, -4, 7, 1, 2.25f);
        tester.testujAlgorytmy(0.8f, true, -4, 7, 1, 2.25f);
    }

    private void testujAlgorytmyTylkoCiagUczacy() {
        //kombinacje parametrow dla rozkladu jednostajnego - nienachodzace na siebie
        tester.testujAlgorytmyTylkoCiagUczacy(0.1f, false, 3, 6, 7, 9);
        tester.testujAlgorytmyTylkoCiagUczacy(0.5f, false, 3, 6, 7, 9);
        tester.testujAlgorytmyTylkoCiagUczacy(0.8f, false, 3, 6, 7, 9);

        //lekko nachodzace na siebie
        tester.testujAlgorytmyTylkoCiagUczacy(0.1f, false, 3, 6, 5.5f, 9);
        tester.testujAlgorytmyTylkoCiagUczacy(0.5f, false, 3, 6, 5.5f, 9);
        tester.testujAlgorytmyTylkoCiagUczacy(0.8f, false, 3, 6, 5.5f, 9);

        //mocno nachodzace na siebie
        tester.testujAlgorytmyTylkoCiagUczacy(0.1f, false, 3, 6, 4, 9);
        tester.testujAlgorytmyTylkoCiagUczacy(0.5f, false, 3, 6, 4, 9);
        tester.testujAlgorytmyTylkoCiagUczacy(0.8f, false, 3, 6, 4, 9);

        //kombinacje parametrow dla rozkladu Gaussa - nienachodzace na siebie
        tester.testujAlgorytmyTylkoCiagUczacy(0.1f, true, -2, 0.5f, 7, 0.5f);
        tester.testujAlgorytmyTylkoCiagUczacy(0.5f, true, -2, 0.5f, 7, 0.5f);
        tester.testujAlgorytmyTylkoCiagUczacy(0.8f, true, -2, 0.5f, 7, 0.5f);

        //lekko nachodzace na siebie
        tester.testujAlgorytmyTylkoCiagUczacy(0.1f, true, -2, 1, 1, 1);
        tester.testujAlgorytmyTylkoCiagUczacy(0.5f, true, -2, 1, 1, 1);
        tester.testujAlgorytmyTylkoCiagUczacy(0.8f, true, -2, 1, 1, 1);

        //mocno nachodzace na siebie
        tester.testujAlgorytmyTylkoCiagUczacy(0.1f, true, -4, 7, 1, 2.25f);
        tester.testujAlgorytmyTylkoCiagUczacy(0.5f, true, -4, 7, 1, 2.25f);
        tester.testujAlgorytmyTylkoCiagUczacy(0.8f, true, -4, 7, 1, 2.25f);
    }

    private void testujAlgorytmyTylkoCiagTestowy() {
        //kombinacje parametrow dla rozkladu jednostajnego - nienachodzace na siebie
        tester.testujAlgorytmyTylkoCiagTestowy(0.1f, false, 3, 6, 7, 9);
        tester.testujAlgorytmyTylkoCiagTestowy(0.5f, false, 3, 6, 7, 9);
        tester.testujAlgorytmyTylkoCiagTestowy(0.8f, false, 3, 6, 7, 9);

        //lekko nachodzace na siebie
        tester.testujAlgorytmyTylkoCiagTestowy(0.1f, false, 3, 6, 5.5f, 9);
        tester.testujAlgorytmyTylkoCiagTestowy(0.5f, false, 3, 6, 5.5f, 9);
        tester.testujAlgorytmyTylkoCiagTestowy(0.8f, false, 3, 6, 5.5f, 9);

        //mocno nachodzace na siebie
        tester.testujAlgorytmyTylkoCiagTestowy(0.1f, false, 3, 6, 4, 9);
        tester.testujAlgorytmyTylkoCiagTestowy(0.5f, false, 3, 6, 4, 9);
        tester.testujAlgorytmyTylkoCiagTestowy(0.8f, false, 3, 6, 4, 9);

        //kombinacje parametrow dla rozkladu Gaussa - nienachodzace na siebie
        tester.testujAlgorytmyTylkoCiagTestowy(0.1f, true, -2, 0.5f, 7, 0.5f);
        tester.testujAlgorytmyTylkoCiagTestowy(0.5f, true, -2, 0.5f, 7, 0.5f);
        tester.testujAlgorytmyTylkoCiagTestowy(0.8f, true, -2, 0.5f, 7, 0.5f);

        //lekko nachodzace na siebie
        tester.testujAlgorytmyTylkoCiagTestowy(0.1f, true, -2, 1, 1, 1);
        tester.testujAlgorytmyTylkoCiagTestowy(0.5f, true, -2, 1, 1, 1);
        tester.testujAlgorytmyTylkoCiagTestowy(0.8f, true, -2, 1, 1, 1);

        //mocno nachodzace na siebie
        tester.testujAlgorytmyTylkoCiagTestowy(0.1f, true, -4, 7, 1, 2.25f);
        tester.testujAlgorytmyTylkoCiagTestowy(0.5f, true, -4, 7, 1, 2.25f);
        tester.testujAlgorytmyTylkoCiagTestowy(0.8f, true, -4, 7, 1, 2.25f);
    }

}
