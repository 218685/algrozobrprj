package com.company;

public class Obraz implements ExtComparable<Obraz> {

    private int klasa;
    private float x;

    public int getKlasa() {
        return klasa;
    }

    public float getX() {
        return x;
    }

    public Obraz(int klasa, float obraz) {
        this.klasa = klasa;
        this.x = obraz;
    }

    public double distanceTo(Obraz drugiObrazk) {
        return Math.pow(this.getX() - drugiObrazk.getX(), 2);
    }

    public int compareTo(Obraz drugiObraz) {
        if (this.x > drugiObraz.x)
            return 1;
        else if (this.x == drugiObraz.x)
            return 0;
        else
            return -1;
    }

    @Override
    public String toString() {

        return String.format("%.2f", x) + "|" + klasa;
    }

}
