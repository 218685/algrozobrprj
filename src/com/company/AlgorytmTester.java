package com.company;

import java.util.Collections;

class AlgorytmTester {

    private static final int ILE_POWTORZEN = 10;
    private int dlugosciCiaguUczacego[] = {10, 100, 1000};
    private double mnoznikiDlaCiaguTestowego[] = {1, 5, 10, 20, 30, 40, 50, 60};
    private GeneratorObrazu gen;

    AlgorytmTester() {
    }

    AlgorytmTester(float p1, boolean rozkladGaussa) {
        gen = new GeneratorObrazu(p1, rozkladGaussa);
    }

    void testujAlgorytmy(float p1, boolean rozkladGaussa, float a1, float b1, float a2, float b2) {

        setParametryTestu(p1, rozkladGaussa, a1, b1, a2, b2);
        dlugosciCiaguUczacego = new int[]{10, 100, 1000};
        mnoznikiDlaCiaguTestowego = new double[]{1, 5, 10, 20, 30, 40, 50, 60};

        System.out.print("\np1 = " + p1);
        System.out.print(rozkladGaussa ? ", rozklad Gaussa | " : ", rozklad jednostajny |");
        System.out.print("a1 = " + a1 + ", b1 = " + b1);
        System.out.println(" | a2 = " + a2 + ", b2 = " + b2);

        double ryzykoBayesa = obliczRyzykoBayesa(gen);
        double ryzykoBayesaWzglednie = ryzykoBayesa / (2 - ryzykoBayesa);
        System.out.println("Ryzyko Bayesa: " + String.format("%.4f", ryzykoBayesa) + " (" + String.format("%.4f", ryzykoBayesaWzglednie) + ").");

        for (int dlugoscCiaguUczacego : dlugosciCiaguUczacego) {
            Ciag ciagUczacy = generujCiagUczacy(dlugoscCiaguUczacego);
            for (double mnoznikDlaCiaguTestowego : mnoznikiDlaCiaguTestowego) {
                int dlugoscCiaguTestowego = (int) (mnoznikDlaCiaguTestowego * dlugoscCiaguUczacego);
                System.out.println("\nN = " + dlugoscCiaguUczacego + ", n = " + dlugoscCiaguTestowego);
                System.out.println("Ryzyko Bayesa: " + String.format("%.4f", ryzykoBayesaWzglednie));
                rozpoznawajObrazyUsredniajac(ciagUczacy, dlugoscCiaguTestowego, ILE_POWTORZEN);
            }
        }
    }

    public void testujAlgorytmyTylkoCiagUczacy(float p1, boolean rozkladGaussa, float a1, float b1, float a2, float b2) {

        setParametryTestu(p1, rozkladGaussa, a1, b1, a2, b2);
        dlugosciCiaguUczacego = new int[]{10, 20, 50, 100, 200, 500, 1000, 2000, 5000, 10000};
        System.out.print("\np1 = " + p1);
        System.out.print(rozkladGaussa ? ", rozklad Gaussa | " : ", rozklad jednostajny |");
        System.out.print("a1 = " + a1 + ", b1 = " + b1);
        System.out.println(" | a2 = " + a2 + ", b2 = " + b2);
        double ryzykoBayesa = obliczRyzykoBayesa(gen);
        double ryzykoBayesaWzglednie = ryzykoBayesa / (2 - ryzykoBayesa);
        System.out.println("Ryzyko Bayesa: " + String.format("%.4f", ryzykoBayesa) + " (" + String.format("%.4f", ryzykoBayesaWzglednie) + ").");

        for (int dlugoscCiaguUczacego : dlugosciCiaguUczacego) {
            Ciag ciagUczacy = generujCiagUczacy(dlugoscCiaguUczacego);

            System.out.println("\nN = " + dlugoscCiaguUczacego + ", n = " + dlugoscCiaguUczacego);
            System.out.println("Ryzyko Bayesa: " + String.format("%.4f", ryzykoBayesaWzglednie));
            rozpoznawajObrazyUsredniajac(ciagUczacy, dlugoscCiaguUczacego, ILE_POWTORZEN);
        }
    }

    public void testujAlgorytmyTylkoCiagTestowy(float p1, boolean rozkladGaussa, float a1, float b1, float a2, float b2) {

        setParametryTestu(p1, rozkladGaussa, a1, b1, a2, b2);
        int dlugoscCiaguUczacego = 1000;
        mnoznikiDlaCiaguTestowego = new double[]{1, 2, 5, 10, 20, 50, 80, 100};
        System.out.print("\np1 = " + p1);
        System.out.print(rozkladGaussa ? ", rozklad Gaussa | " : ", rozklad jednostajny |");
        System.out.print("a1 = " + a1 + ", b1 = " + b1);
        System.out.println(" | a2 = " + a2 + ", b2 = " + b2);

        double ryzykoBayesa = obliczRyzykoBayesa(gen);
        double ryzykoBayesaWzglednie = ryzykoBayesa / (2 - ryzykoBayesa);
        System.out.println("Ryzyko Bayesa: " + String.format("%.4f", ryzykoBayesa) + " (" + String.format("%.4f", ryzykoBayesaWzglednie) + ").");

        Ciag ciagUczacy = generujCiagUczacy(dlugoscCiaguUczacego);
        for (double mnoznikDlaCiaguTestowego : mnoznikiDlaCiaguTestowego) {
            int dlugoscCiaguTestowego = (int) (mnoznikDlaCiaguTestowego * dlugoscCiaguUczacego);
            System.out.println("\nN = " + dlugoscCiaguUczacego + ", n = " + dlugoscCiaguTestowego);
            System.out.println("Ryzyko Bayesa: " + String.format("%.4f", ryzykoBayesaWzglednie));
            rozpoznawajObrazyUsredniajac(ciagUczacy, dlugoscCiaguTestowego, ILE_POWTORZEN);
        }
    }

    private void setParametryTestu(float p1, boolean rozkladGaussa, float a1, float b1, float a2, float b2) {

        gen = new GeneratorObrazu(p1, rozkladGaussa);
        gen.setParametryRozkladu(a1, b1, a2, b2);
    }

    private double obliczRyzykoBayesa(GeneratorObrazu gen) {
        return Algorytm.ryzykoBayesa(gen);
    }

    private Ciag generujCiagUczacy(int N) {

        Ciag ciagUczacy = new Ciag(N);

        for (int i = 0; i < N; i++) {
            ciagUczacy.dodajNowyObraz(gen.generujObraz());
        }

        Collections.sort(ciagUczacy.getListaObrazow());
        return ciagUczacy;
    }

    private void rozpoznawajObrazyUsredniajac(Ciag ciagUczacy, int n, int ilePowtorzen) {

        double sredniaCzestoscBledowNM = 0;
        double sredniaCzestoscBledow1NN = 0;
        double sredniaCzestoscBledow3NN = 0;
        double sredniaCzestoscBledow5NN = 0;
        double sredniaCzestoscBledowBayesa = 0;

        for (int i = 0; i < ilePowtorzen; i++) {

            Ciag ciagTestowy = new Ciag(n);

            for (int j = 0; j < n; j++) {
                ciagTestowy.dodajNowyObraz(gen.generujObraz());
            }

            int bledyNM = Algorytm.najblizszaSrednia(ciagUczacy, ciagTestowy);
            int bledy1NN = Algorytm.alfaNajblizszySasiad(1, ciagUczacy, ciagTestowy);
            int bledy3NN = Algorytm.alfaNajblizszySasiad(3, ciagUczacy, ciagTestowy);
            int bledy5NN = Algorytm.alfaNajblizszySasiad(5, ciagUczacy, ciagTestowy);
            int bledyBayesa = Algorytm.klasyfikatorBayesa(ciagTestowy, gen);

            sredniaCzestoscBledowNM += (double) bledyNM / n;
            sredniaCzestoscBledow1NN += (double) bledy1NN / n;
            sredniaCzestoscBledow3NN += (double) bledy3NN / n;
            sredniaCzestoscBledow5NN += (double) bledy5NN / n;
            sredniaCzestoscBledowBayesa += (double) bledyBayesa / n;
        }
        //usrednianie
        sredniaCzestoscBledowNM = sredniaCzestoscBledowNM / ilePowtorzen;
        sredniaCzestoscBledow1NN = sredniaCzestoscBledow1NN / (ilePowtorzen);
        sredniaCzestoscBledow3NN = sredniaCzestoscBledow3NN / (ilePowtorzen);
        sredniaCzestoscBledow5NN = sredniaCzestoscBledow5NN / (ilePowtorzen);
        sredniaCzestoscBledowBayesa = sredniaCzestoscBledowBayesa / (ilePowtorzen);

        System.out.println("Algorytm NM: " + String.format("%.4f", sredniaCzestoscBledowNM));
        System.out.println("Algorytm 1NN: " + String.format("%.4f", sredniaCzestoscBledow1NN));
        System.out.println("Algorytm 3NN: " + String.format("%.4f", sredniaCzestoscBledow3NN));
        System.out.println("Algorytm 5NN: " + String.format("%.4f", sredniaCzestoscBledow5NN));
        System.out.println("Algorytm Bayesa: " + String.format("%.4f", sredniaCzestoscBledowBayesa));
    }

    private void rozpoznawajObrazy(Ciag ciagUczacy, int n) {

        Ciag ciagTestowy = new Ciag(n);

        for (int i = 0; i < n; i++) {
            ciagTestowy.dodajNowyObraz(gen.generujObraz());
        }

        int bledyNM = Algorytm.najblizszaSrednia(ciagUczacy, ciagTestowy);
        int bledy1NN = Algorytm.alfaNajblizszySasiad(1, ciagUczacy, ciagTestowy);
        int bledy3NN = Algorytm.alfaNajblizszySasiad(3, ciagUczacy, ciagTestowy);
        int bledy5NN = Algorytm.alfaNajblizszySasiad(5, ciagUczacy, ciagTestowy);
        int bledyBayesa = Algorytm.klasyfikatorBayesa(ciagTestowy, gen);

        double czestoscBledowNM = (double) bledyNM / n;
        double czestoscBledow1NN = (double) bledy1NN / n;
        double czestoscBledow3NN = (double) bledy3NN / n;
        double czestoscBledow5NN = (double) bledy5NN / n;
        double czestoscBledowBayesa = (double) bledyBayesa / n;

        System.out.println("Algorytm NM pomylił się: " + bledyNM + " razy (" + String.format("%.4f", czestoscBledowNM) + ").");
        System.out.println("Algorytm NN " + 1 + " pomylił się: " + bledy1NN + " razy (" + String.format("%.4f", czestoscBledow1NN) + ").");
        System.out.println("Algorytm NN " + 3 + " pomylił się: " + bledy3NN + " razy (" + String.format("%.4f", czestoscBledow3NN) + ").");
        System.out.println("Algorytm NN " + 5 + " pomylił się: " + bledy5NN + " razy (" + String.format("%.4f", czestoscBledow5NN) + ").");
        System.out.println("Klasyfikator Bayesa pomylił się: " + bledyBayesa + " razy (" + String.format("%.4f", czestoscBledowBayesa) + ").");
    }


}
