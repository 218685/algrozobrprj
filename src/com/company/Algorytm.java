package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import static java.lang.StrictMath.abs;

public class Algorytm {

    static int najblizszaSrednia(Ciag ciagUczacy, Ciag ciagTestowy) {

        //wyliczanie centrów klas
        float centrumKlasy1 = 0, centrumKlasy2 = 0;
        int sumaObrazowKlasy1 = 0, sumaObrazowKlasy2 = 0;

        for (Obraz o : ciagUczacy.getListaObrazow()) {

            if (o.getKlasa() == 1) {
                sumaObrazowKlasy1++;
                centrumKlasy1 += o.getX();
            }

            if (o.getKlasa() == 2) {
                sumaObrazowKlasy2++;
                centrumKlasy2 += o.getX();
            }
        }

        centrumKlasy1 /= sumaObrazowKlasy1;
        centrumKlasy2 /= sumaObrazowKlasy2;

        int klasaObrazuTestowego, liczbaBledow = 0;
        float odlegloscOdKlasy1, odlegloscOdKlasy2;

        for (Obraz o : ciagTestowy.getListaObrazow()) {

            //norma euklidesowa
            odlegloscOdKlasy1 = (float) Math.pow((o.getX() - centrumKlasy1), 2);
            odlegloscOdKlasy2 = (float) Math.pow((o.getX() - centrumKlasy2), 2);

            //algorytm dobiera klasę
            if (odlegloscOdKlasy1 < odlegloscOdKlasy2)
                klasaObrazuTestowego = 1;
            else
                klasaObrazuTestowego = 2;

            //zliczamy błędne dopasowania klasy do obrazu
            if (klasaObrazuTestowego != o.getKlasa())
                liczbaBledow++;
        }

        return liczbaBledow;

    }

    static int alfaNajblizszySasiad(int alfa, Ciag ciagUczacy, Ciag ciagTestowy) {

        int klasaObrazuTestowego, liczbaBledow = 0;

        for (Obraz obrazTestowy : ciagTestowy.getListaObrazow()) {
            List<Obraz> listaSasiadow = Algorytm.getListaSasiadowAlfa(alfa, obrazTestowy, ciagUczacy);

            int ileSasiadowKlasy1 = 0, ileSasiadowKlasy2 = 0;

            //porownujemy, czy obrazowi testowemu blizej do klasy 1 czy 2
            for (Obraz sasiad : listaSasiadow) {
                if (sasiad.getKlasa() == 1)
                    ileSasiadowKlasy1++;
                else
                    ileSasiadowKlasy2++;
            }

            if (ileSasiadowKlasy1 > ileSasiadowKlasy2)
                klasaObrazuTestowego = 1;
            else
                klasaObrazuTestowego = 2;

            //sprawdzenie czy dobrze okreslona klasa
            if (klasaObrazuTestowego != obrazTestowy.getKlasa())
                liczbaBledow++;
        }

        return liczbaBledow;
    }

    private static List<Obraz> getListaSasiadowAlfa(int alfa, Obraz obrazTestowy, Ciag ciagUczacy) {

        int indexNajblizszegoSasiada = znajdzIndexNajblizszegoSasiada(obrazTestowy, ciagUczacy);
        return getListaSasiadow(alfa, ciagUczacy, indexNajblizszegoSasiada);
    }

    private static List<Obraz> getListaSasiadow(int alfa, Ciag ciagUczacy, int indexNajblizszegoSasiada) {
        List<Obraz> listaSasiadow = new ArrayList<>(alfa);

        int poczatek, koniec;
        if (indexNajblizszegoSasiada + alfa / 2 >= ciagUczacy.size() - 1) {
            indexNajblizszegoSasiada -= alfa / 2;
        }
        if (indexNajblizszegoSasiada - alfa / 2 <= 0) {
            indexNajblizszegoSasiada += alfa / 2;
        }

        poczatek = indexNajblizszegoSasiada - alfa / 2;
        koniec = indexNajblizszegoSasiada + alfa / 2;

        for (int i = poczatek; i <= koniec; i++) {
            Obraz sasiad = ciagUczacy.getListaObrazow().get(i);
            listaSasiadow.add(sasiad);
        }
        return listaSasiadow;
    }

    private static int znajdzIndexNajblizszegoSasiada(Obraz obrazTestowy, Ciag ciagUczacy) {

        ArrayList<Obraz> listaObrazow = ciagUczacy.getListaObrazow();

        int indexNajblizszegoSasiada = Collections.binarySearch(listaObrazow, obrazTestowy);
        indexNajblizszegoSasiada = Math.abs(indexNajblizszegoSasiada + 1);

        if (indexNajblizszegoSasiada >= listaObrazow.size())
            indexNajblizszegoSasiada = listaObrazow.size() - 1;

        double currentDistance, minDistance = 99999;

        int poczatek = indexNajblizszegoSasiada - 1;
        int koniec = indexNajblizszegoSasiada + 1;
        if (indexNajblizszegoSasiada + 1 > listaObrazow.size())
            koniec = indexNajblizszegoSasiada;
        if (indexNajblizszegoSasiada - 1 < 0)
            poczatek = indexNajblizszegoSasiada;
        for (int i = poczatek; i < koniec; i++) {
            currentDistance = listaObrazow.get(i).distanceTo(obrazTestowy);
            if (currentDistance < minDistance) {
                minDistance = currentDistance;
                indexNajblizszegoSasiada = i;
            }
        }

        return indexNajblizszegoSasiada;
    }

    @Deprecated
    private static int getIndexNajblizszegoSasiada(Obraz obrazTestowy, Ciag ciagUczacy) {
        double minDistance = 99999, currentDistance;
        int indexNajblizszegoSasiada = 0;

        for (Obraz obrazUczacy : ciagUczacy.getListaObrazow()) {
            currentDistance = obrazUczacy.distanceTo(obrazTestowy);
            if (currentDistance < minDistance) {
                minDistance = currentDistance;
                indexNajblizszegoSasiada = ciagUczacy.getListaObrazow().indexOf(obrazUczacy);
            }
        }
        return indexNajblizszegoSasiada;
    }

    private static float gestoscGaussa(float x, float a, float b) {

        double f_x = 1 / b / (Math.sqrt(2 * Math.PI));
        double wykladnik = -Math.pow(x - a, 2) / 2 / Math.pow(b, 2);
        f_x *= Math.pow(Math.E, wykladnik);

        return (float) f_x;
    }

    private static double gestoscGaussa(double x, float a, float b) {

        double f_x = 1 / b / (Math.sqrt(2 * Math.PI));
        double wykladnik = -Math.pow(x - a, 2) / 2 / Math.pow(b, 2);
        f_x *= Math.pow(Math.E, wykladnik);

        return f_x;
    }

    private static float gestoscJednostajna(float x, float a, float b) {

        if (a <= x && x <= b)
            return 1 / (b - a);
        else
            return 0;
    }

    public static int klasyfikatorBayesa(Ciag ciagTestowy, GeneratorObrazu gen) {

        float p1 = gen.getP1();
        float p2 = 1 - p1;
        int klasaObrazuTestowego, liczbaBledow = 0;

        float obraz1, obraz2;

        boolean czyRozkladGaussa = gen.isRozkladGaussa();
        float a1 = gen.getA1(), b1 = gen.getB1(), a2 = gen.getA2(), b2 = gen.getB2();

        for (Obraz obrazTestowy : ciagTestowy.getListaObrazow()) {

            if (czyRozkladGaussa) {
                obraz1 = p1 * gestoscGaussa(obrazTestowy.getX(), a1, b1);
                obraz2 = p2 * gestoscGaussa(obrazTestowy.getX(), a2, b2);
            } else {
                obraz1 = p1 * gestoscJednostajna(obrazTestowy.getX(), a1, b1);
                obraz2 = p2 * gestoscJednostajna(obrazTestowy.getX(), a2, b2);
            }

            if (obraz1 >= obraz2)
                klasaObrazuTestowego = 1;
            else
                klasaObrazuTestowego = 2;

            if (klasaObrazuTestowego != obrazTestowy.getKlasa())
                liczbaBledow++;
        }

        return liczbaBledow;
    }

    public static double ryzykoBayesa(GeneratorObrazu gen) {
        double ryzykoBayesa = 0;
        double xG = 0;

        float p1 = gen.getP1();
        float p2 = gen.getP2();
        float a1 = gen.getA1();
        float b1 = gen.getB1();

        float a2 = gen.getA2();
        float b2 = gen.getB2();

        if (gen.isRozkladGaussa()) {
            double kraniecPrawegoRozkladu = a1 + 3 * b1;
            double kraniecLewegoRozkladu = a2 - 3 * b2;

            if (kraniecPrawegoRozkladu >= kraniecLewegoRozkladu) {
                //znajdujemy punkt graniczny rozkladu metoda bisekcji
                double prawyKraniec = kraniecLewegoRozkladu;
                double lewyKraniec = kraniecPrawegoRozkladu;
                double f_xG, f_prawyKraniec;
                double DOKLADNOSC = 1.0E-04;

                while (Math.abs(prawyKraniec - lewyKraniec) > DOKLADNOSC) {
                    xG = (prawyKraniec + lewyKraniec) / 2;
                    f_xG = funkcjaWypadkowaRozkladowGaussa(xG, gen);
                    f_prawyKraniec = funkcjaWypadkowaRozkladowGaussa(prawyKraniec, gen);
                    if (abs(f_xG) <= DOKLADNOSC) {
                        break;
                    }
                    if ((f_xG * f_prawyKraniec) < 0) {
                        lewyKraniec = xG;
                    } else {
                        prawyKraniec = xG;
                    }
                }

                //liczymy pole wspolne rozkladow prawdop.
                double polePrawejCzesciWspolnej = 0, step = 0.1;
                prawyKraniec = xG;
                double poleProstokata, f_x;
                while (prawyKraniec >= kraniecLewegoRozkladu) {
                    prawyKraniec -= step;
                    f_x = p2 * gestoscGaussa(prawyKraniec + 0.5 * step, a2, b2);
                    poleProstokata = f_x * step;
                    polePrawejCzesciWspolnej += poleProstokata;
                }
                double poleLewejCzesciWspolnej = 0;
                lewyKraniec = xG;
                while (lewyKraniec <= kraniecPrawegoRozkladu) {
                    lewyKraniec += step;
                    f_x = p1 * gestoscGaussa(lewyKraniec - 0.5 * step, a1, b1);
                    poleProstokata = f_x * step;
                    poleLewejCzesciWspolnej += poleProstokata;
                }

                ryzykoBayesa = polePrawejCzesciWspolnej + poleLewejCzesciWspolnej;
            }
        } else {
            //rozklad jednostajny
            if (b1 >= a2) {
                xG = a2;
                float f_a2 = p1 * gestoscJednostajna((float) xG, a1, b1);
                float b1_a2 = b1 - a2;

                ryzykoBayesa = f_a2 * b1_a2;
            } else {
                ryzykoBayesa = 0;
            }
        }

        return ryzykoBayesa;
    }

    private static double funkcjaWypadkowaRozkladowGaussa(double xG, GeneratorObrazu gen) {

        //p1f1(x)-p2f2(x)
        float p1 = gen.getP1();
        float p2 = gen.getP2();

        float a1 = gen.getA1();
        float b1 = gen.getB1();

        float a2 = gen.getA2();
        float b2 = gen.getB2();

        double pierwszyRozklad = p1 * gestoscGaussa(xG, a1, b1);
        double drugiRozklad = p2 * gestoscGaussa(xG, a2, b2);

        return pierwszyRozklad - drugiRozklad;
    }
}
