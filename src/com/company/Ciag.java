package com.company;

import java.util.ArrayList;

public class Ciag {

    private ArrayList<Obraz> listaObrazow;

    public ArrayList<Obraz> getListaObrazow() {
        return listaObrazow;
    }

    public Ciag(){
        listaObrazow = new ArrayList<>();
    }

    public Ciag(int dlugosc) {
        listaObrazow = new ArrayList<>(dlugosc);
    }

    public void dodajNowyObraz(Obraz obraz){
        listaObrazow.add(obraz);
    }

    public int size() {
        return getListaObrazow().size();
    }

    @Override
    public String toString(){

        StringBuilder napis = new StringBuilder();

        for (Obraz o: listaObrazow) {
            napis.append(o.toString());
            napis.append(" ");
        }

        return napis.toString();
    }
}
